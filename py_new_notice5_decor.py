import time


def wraper(decor: str):
    def timer(func):
        def fet_arg(nu):
            t_start = time.time()
            factor = func(nu)
            t_stop = time.time()
            t_elapsed = t_stop - t_start
            return f'{decor} start_time: {t_start}, {decor} stop_time: {t_stop}, {decor} time elapsed: {t_elapsed}, {decor} factorial: {factor}'
        return fet_arg
    return timer


@wraper('\n')
def factorial(n):
	step=1
	for v in range(1,n+1):
		step *=v
	return step

print(factorial(100000))
